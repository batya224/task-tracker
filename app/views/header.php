<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
<!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
    <!-- BOOTSTRAP STYLE -->
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>/public/css/bootstrap.min.css">
    <!-- JQUERY SCRIPT -->
<!--    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>-->

    <!-- POPPER SCRIPT -->
<!--    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"-->
<!--            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"-->
<!--            crossorigin="anonymous">-->
<!--    </script>-->
<!--    -->

    <!-- BOOTSTRAP SCRIPT -->
    <script src="<?php echo BASE_URL; ?>/public/js/bootstrap.min.js">
    </script>
    <!-- FONTAWESOME STYLES -->
    <!--        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"-->
    <!--            crossorigin="anonymous">-->
    <!-- CUSTOM STYLES -->
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>/public/css/style.css">
    <title>
        <?php echo SITE_NAME; ?>
    </title>
</head>

<body>
<header>
    <?php require_once 'inc/navbar.php'; ?>
</header>

<div id="main-content">
